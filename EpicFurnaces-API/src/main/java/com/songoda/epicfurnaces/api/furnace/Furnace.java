package com.songoda.epicfurnaces.api.furnace;

import org.bukkit.Location;

import java.util.List;
import java.util.UUID;

public interface Furnace {
    Level getLevel();

    List<UUID> getAccessList();

    int getPerformanceTotal();

    List<String> getOriginalAccessList();

    boolean addToAccessList(String string);

    boolean removeFromAccessList(String string);

    void clearAccessList();

    Location getLocation();

    UUID getPlacedBy();

    void setNickname(String nickname);

    String getNickname();

    List<Location> getRadius(boolean overHeat);

    void addToRadius(Location location, boolean overHeat);

    void clearRadius(boolean overHeat);

    int getRadiusLast(boolean overHeat);

    void setRadiusLast(int radiusLast, boolean overHeat);

    int getUses();

    int getTolevel();
}
